# GitLab CI/CD Template for .NET 6.0

This project contains the GitLab CI/CD templates that are used
(via the [include](https://docs.gitlab.com/ee/ci/yaml/#include) keyword)
in other projects, including sequence/templates/dotnetlibrary>.

# How to use

Add a `.gitlab-ci.yml` file to the root of your project, with the following:

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.linux.yml
```

The _file_ should either be `.gitlab-ci.linux.yml` or `.gitlab-ci.windows.yml`
depending on the runners that you'd like to use for your project.

By default, this project's default branch is used for the template.
You can also add the _ref_ keyword to use a specific version
(tag/branch/commit) of the CI files.

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.windows.yml
    ref: v0.1.0
```

## .NET 5

The last release to support .NET 5 is `v0.8.0`. To use this CI pipeline:

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.linux.yml
    ref: v0.8.0
```

Current releases might be compatible with .net 5 projects, but are not supported.

## .NET Core 3.1

The last release to support .NET Core 3.1 is `v0.3.0`. To use this CI pipeline:

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.linux.yml
    ref: v0.3.0
```

## Default Rules and Scripts

Are specified in `.gitlab/ci/defaults.yml`.

Rules are applied to jobs using the `extends` keyword. Since
extends does not support merging arrays of values, multiple rule
sets cannot be used with one job.

The default `before_script` adds the Sequence nuget repository
to all the jobs that access it.

## Customising

To overwrite anything in the templates, just add that keyword to your
ci file. For example, to set the package names in your CI file:

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.linux.yml

variables:
  PACKAGE_NAME_NUGET: CustomNugetName
  PACKAGE_NAME_DLL: CustomDllName
```

To overwrite something specific to a job, add the job to the CI file,
and overwrite keywords as required. For example, to make the
_mutation testing_ job only execute if the build and test stages are
successful, add the following:

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.linux.yml

mutation testing:
  needs:
    - build dev
    - test dev
```

And to overwrite any of the job rules specified in the `defaults.yml`:

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab-ci.linux.yml

.rules_manual:
  rules:
    - when: manual
      allow_failure: true
```

An example is `.gitlab-ci.windows.yml` where the `default` keyword is
used to define the `rt-windows` tag for all the jobs and the _code_quality_
job has its tags overwritten.

## Connectors

For [Sequence Connectors](https://gitlab.com/sequence/connectors),
there are additional jobs available to package, sign and push connector packages to the
[Connector Registry](https://gitlab.com/sequence/connector-registry).

To enable these jobs, add `PUBLISH_CONNECTOR` to the variables block:

```yaml
variables:
  PUBLISH_CONNECTOR: 1
```

## Full customisation

To fully customise the stages, workflow, and defaults, it's possible
to select which individual _default_ and _stage_ files are included
in your CI configuration.

```yaml
include:
  - project: sequence/templates/cicd
    file: .gitlab/ci/default.default.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/default.rules.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/default.scripts.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/default.stages.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/default.variables.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/default.workflow.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/build.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/test.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/quality.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/package.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/sign.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/push.yml
  - project: sequence/templates/cicd
    file: .gitlab/ci/connectors.yml
```

## Available Runners

|       Tag        |   OS    | Description                                                                                             |
| :--------------: | :-----: | :------------------------------------------------------------------------------------------------------ |
| No tag specified |  Linux  | GitLab's shared linux runners that use the `docker+machine` executor and support dind.                  |
|    `windows`     | Windows | GitLab's shared windows runners. They use a `custom` executor with powershell 5.1 as the default shell. |
|    `rt-linux`    |  Linux  | Sequence group linux runner. Uses the `docker` executor.                                                |
|   `rt-windows`   | Windows | Sequence group windows runner. Server 2019 (1809) with `docker-windows` executor.                       |
|    `rt-dind`     |  Linux  | Sequence group runner that supports dind.                                                               |

_dind_ is Docker-in-Docker and allows docker images to be built.

See [here](https://docs.gitlab.com/ee/user/gitlab_com/index.html#shared-runners)
for more information on the GitLab shared runners.

## Caching

A local cache server is available when using the `rt` runners. To enable,
add a global cache block with the paths that you want to cache:

```yaml
cache:
  paths:
    - packages/
```

See [GitLab docs](https://docs.gitlab.com/ee/ci/caching/) for more information,
including the difference between _cache_ and _artifacts_.

# The CI pipeline

## Rules

The pipelines is defined to run for:

- All commits for the default branch
- Merge request commits
- All tags

Workflow rules are defined in [.gitlab-ci.yml](.gitlab-ci.yml), and
the default rules that are used for various jobs are defined in
[defaults.yml](.gitlab/ci/defaults.yml).

## Stages and jobs

The CI pipeline has six [stages](.gitlab/ci/default.stages.yml)

- [build](.gitlab/ci/build.yml)
- [test](.gitlab/ci/test.yml)
- [quality](.gitlab/ci/quality.yml)
- [package](.gitlab/ci/package.yml)
- [sign](.gitlab/ci/sign.yml)
- [push](.gitlab/ci/push.yml)

Each stage has one or more jobs, most have at least two because
of development and release jobs. The major differences between
_dev_ and _release_ jobs are:

- Project is built using `Debug` configuration for _dev_ and `Release` for _release_
- The development nuget feed _nuget-dev_ is used for _dev_ and the release feed _nuget_ for _release_
- Artifacts never expire for _release_ jobs

### build

Does a restore and a build using either the `Debug` or `Release` configuration.
Stores packages and build output, and makes it available to all
subsequent jobs, so that the restore and build only has to take place once.

The jobs in this stage will use different nuget feeds - so a build
will only be successful if it's using packages available in the
dev/release feed. This means that release packages can only be built
using other release packages.

### test

Runs `dotnet test` using either the `Debug` or `Release` configuration.
Requires `coverlet.collector` to get code coverage metrics.
_Coverlet_ produces a `coverage.cobertura.xml` report, and a script
is then used to retrieve code coverage metrics from this report. The
script also fixes compatibility issues with GitLab and the way _coverlet_
outputs paths.

[More info on the _cobertura_ reports and test coverage visualization.](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html)

The _version check_ jobs are also run as part of the test stage.
If a release project version does not match the release tag, this stage fails.

### quality

Runs quality and security jobs.

#### Code Coverage

The `code coverage` job combines any reports that were generated during the `test`
stage and match `coverage.*.xml` and then uploads that report to [codecov.io](https://codecov.io).

#### GitLab Quality and Security Jobs

This stage runs the following jobs:

- [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#code-quality)
- [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Secrets Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)

Code quality is an optional job for merge requests. The other two run for
merge requests and the default branch.

Use the following variables to disable quality jobs for you project:

- `CODE_QUALITY_DISABLED`
- `SAST_DISABLED`
- `SECRET_DETECTION_DISABLED`

#### Mutation Testing

This stage can also run on-demand mutation testing using [Stryker](https://stryker-mutator.io/)
and produce a Mutation Report which is made available in Merge Requests.

Mutation testing runs automatically on the default branch but is allowed
to fail as it can be a bit temperamental on some projects.

### package

Creates a nuget and dll package for the project using either the
`Debug` or `Release` configuration. There are two nuget jobs.
Dev uses the `Debug` configuration. Release uses the `Release`
configuration and its artifacts never expire.

On release branches, an additional job runs that creates a package
of the library dll. This is made available for download on the
releases page.

### sign

This stage only runs for release pipelines. It's used to sign
nuget packages.

### push

Contains jobs that push the nuget package to the Sequence and nuget.org
package feeds.

- Releases are pushed to both _nuget_ and _nuget-dev_
- Default branch builds are pushed to _nuget-dev_
- Dev builds are also pushed to _nuget-dev_

#### Push to nuget.org

To automatically push releases to [nuget.org](https://www.nuget.org/):

- Set the `PUBLISH_NUGETORG` variable in the project's `.gitlab-ci.yml`
- Add `NUGETORG_API_KEY` variable (protected & masked) in GitLab's CI/CD
  settings for the project

# Versioning

The CI jobs that are responsible for the nuget package versions
are `version check dev` and `version check release`.

This job adds the following environment variables to any subsequent jobs:

|     Variable      | Description                                                                                                                                                   |
| :---------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------ |
|  PROJECT_VERSION  | This is the `Version` property of the _csproj_ file. Test projects are ignored. If more than one non-test project exists, the last project's version is used. |
|  VERSION_SUFFIX   | This is generated by the job and will be appended to the project version.                                                                                     |
| VERSION_TIMESTAMP | Time of the build in "yyMMddHHmm" format. It's used as a build version.                                                                                       |

> :exclamation: This job will fail a release pipeline if a release tag does not match the version number.

## VersionSuffix

The _VersionSuffix_ consists of:

- A pre-release suffix: if the project version already has a pre-release suffix
  (e.g. _alpha_, _rc_) then that's used, otherwise the suffix defined by the
  `DEFAULT_PRERELEASE_VERSION` CI variable is used.
- Either '.main' on the _default_ branch, or '.dev' for all other commits
- The version timestamp. Date used for the following examples is _2020/10/30 09:45_.

If the _Version_ in the csproj file is `1.1.0`:

- A build for a merge request, will have version number `1.1.0-a.dev.2010300945`
- A build on the _default_ branch will have version number `1.1.0-a.main.2010300945`
- A release build will have version `1.1.0`

If the _Version_ in the csproj file is `1.1.0-beta`:

- A build for a merge request, will have version number `1.1.0-beta.dev.2010300945`
- A build on the _default_ branch will have version number `1.1.0-beta.main.2010300945`
- A release build will have version `1.1.0-beta`
