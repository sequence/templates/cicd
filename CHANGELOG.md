# v0.9.0 (2022-07-06)

## Issues Closed in this Release

### New Features

- Set a retry on package/push jobs if possible #43
- Push release packages to nuget #51
- Enable Source Link #53
- Create a dev repository for connectors #50
- Enable push to connector registry for main branch #48

### Bug Fixes

- Replace cobertura report with code coverage #52
- sign stage needs powershell to run #49

### Maintenance

- Update code coverage windows image to .net 6 #47
- Update to work with .net 6 #44

# v0.8.0 (2021-12-09)

Last release to support .net 5.

## Issues Closed in this Release

### New Features

- Add security jobs to quality stage #36
- Check for existing releases in version check job #40
- Update CI to work with master or main branches #39

### Maintenance

- Replace references to master with CI_DEFAULT_BRANCH variable #38

## v0.7.1 (2021-06-24)

- Build job should inherit default cache values #32

## v0.7.0 (2021-06-23)

- Cache for jobs other than build should be pull only #29
- Use global stryker tool for mutation testing job #28
- Exclude designer files from code coverage #27
- Connector pack stage needs reductech registry for restore to work #26
- Connector packages should include all dependencies #25

## v0.6.0 (2021-05-13)

- Add job to publish connectors to the connector registry #24

## v0.5.0 (2021-04-08)

- Codecov uploader should correctly set the branch name #23
- Add codecov job to quality stage #22
- Update default sdk image #21

## v0.4.2 (2021-01-15)

- Coverage report should be displayed in merge requests #17

## v0.4.1 (2021-01-06)

- Allow mutation testing job to fail on master branch #18

## v0.4.0 (2020-12-11)

- Update windows and linux images to .NET 5.0

## v0.3.0 (2020-11-17)

### New Features

- Add job or stage to sign binaries and nuget packages #11

### Bug Fixes

- Push stage should work with multiple packages #10

## v0.2.0 (2020-11-06)

### New Features

- Move default, stages, workflow, and variables to separate files #7
- Change artifact expiration on master branch #6

### Bug Fixes

- 'push to nuget dev' job should not run for releases #9

## v0.1.0 (2020-11-05)

Initial release of the dotnet CI project, which contains
the CI configuration taken from `v0.2.0` of
[reductech/sandbox/templates/dotnetlibrary](https://gitlab.com/reductech/sandbox/templates/dotnetlibrary/-/tags/v0.2.0)
and the following additional features.

### New Features

- Add caching for nuget packages #4
- Create new CI config for windows / rt runners #1

### Documentation

- Add readme with instructions on how to use the templates #2
